package com.revisit.collection;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Ansari on 12/16/2018
 */
public class LinkedListExample {

    public static void main(String[] args) {

        LinkedList states = new LinkedList();
         states.add("Maharashtra");
         states.add("Gujrat");
         states.add("Goa");
         states.add("Hyderabad");

         states.addFirst("Kashmir");

        System.out.println(states);
        System.out.println("Last state: " + states.getLast());

        ListIterator iterator = states.listIterator(states.size());

        while(iterator.hasPrevious()){
            System.out.println(iterator.previous());
        }

    }
}
