package com.revisit.collection;

import java.util.Stack;

/**
 * @author Ansari on 12/16/2018
 */
public class StackExample {

    public static void main(String[] args) {

        Stack stack = new Stack();

        for (int i = 0; i <= 10 ; i++) {
            stack.push(i);
        }

        while(!stack.empty()){
            System.out.print(stack.pop());
            System.out.print(",");
        }

        System.out.print("LIFT OFF");
    }
}
