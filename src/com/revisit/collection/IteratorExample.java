package com.revisit.collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Ansari on 12/16/2018
 */

public class IteratorExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Collection collection = Arrays.asList("Red","Yellow","Orange","Blue","Violet","Purple");

        Iterator iterator = collection.iterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }

}
