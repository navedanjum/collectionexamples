package com.revisit.collection;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Ansari on 12/16/2018
 */
public class CollectionExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<Integer> list1 = Arrays.asList(1,1,2,3,5,8,13,21,34,55);
        System.out.println("Posiion of 13 is "+ Collections.binarySearch(list1,21));

    }

}
