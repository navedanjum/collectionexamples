package com.revisit.challenge1;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Ansari on 12/16/2018
 */
public class GroceryList {

    public static void main(String[] args) {

        ArrayList<String> grocery1 = new ArrayList<>();
        ArrayList<String> grocery2 = new ArrayList<>(Arrays.asList("Mango","Apple","Banana"));

        grocery1.add("Tea");
        grocery1.add("Eggs");
        grocery1.add("Milk");
        grocery1.add("Choclate");

        System.out.println(grocery1);

/*
        grocery2.add("Mango");
        grocery2.add("Apple");
        grocery2.add("Banana");
*/

        System.out.println(grocery2);

        grocery1.add("Books");
        grocery1.add("BinBags");

        grocery1.addAll(grocery2);
        System.out.println(grocery1);

        int index = grocery1.indexOf("Books");
        System.out.println(index);

        grocery1.remove(index);
        System.out.println(grocery1);

    }

}
