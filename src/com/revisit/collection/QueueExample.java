package com.revisit.collection;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Ansari on 12/16/2018
 */
public class QueueExample {

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        for(int i=0; i <11 ; i++){
             queue.add(i);
        }

        System.out.println("Elements in queue "+ queue);
        int removed = queue.remove();

        System.out.println("Element removed is "+ removed);

        int top = queue.peek();
        System.out.println("Top element is "+ top);

        System.out.println(queue);
    }



}
