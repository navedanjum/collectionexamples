package com.revisit.challenge2;

public interface Space extends Vehicle {

    boolean getIsTaken();
    void setIsTaken(boolean isTaken);

}